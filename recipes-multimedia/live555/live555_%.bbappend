do_configure() {
    cp ${WORKDIR}/config.linux-cross .
    echo "COMPILE_OPTS+=" -fPIC -DXLOCALE_NOT_USED -DNO_OPENSSL=1"" >> config.linux-cross
    ./genMakefiles linux-cross
}
