# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)

# WARNING: the following LICENSE and LIC_FILES_CHKSUM values are best guesses - it is
# your responsibility to verify that the values are complete and correct.
#
# The following license files were not able to be identified and are
# represented as "Unknown" below, you will need to check them yourself:
#   surveillance/demo/resources/LICENSE.txt
#
# NOTE: multiple licenses have been detected; they have been separated with &
# in the LICENSE value for now since it is a reasonable assumption that all
# of the licenses apply. If instead there is a choice between the multiple
# licenses then you should change the value to separate the licenses with |
# instead of &. If there is any doubt, check the accompanying documentation
# to determine which situation is applicable.
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://LICENSE;md5=a23a74b3f4caf9616230789d94217acb \
                    file://surveillance/demo/resources/LICENSE.txt;md5=9902697e1c50fdef8c133605af37b96b"

PV = "v3_latest"

SRC_URI = "git://github.com/SvenVD/rpisurv/;protocol=https;branch=${PV} \
           file://0001-Fix_yaml_load.patch \
           file://0003-fix_fullscreen.patch \
           file://display1.yml"
#           file://0002-fix_display.patch 

SRCREV = "757dd9180d69d33377df422f4d42395e71cb483c"

RDEPENDS:${PN} = "vlc rsync sed coreutils fbset ffmpeg openssl procps python3 python3-pyyaml python3-pyopenssl python3-pygame "
RDEPENDS:${PN} += " bash libpng userland"
# libraspberrypi-bin python3-openssl python3-pygame python3-yaml
S = "${WORKDIR}/git"

inherit setuptools3 systemd

SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_SERVICE:${PN} = "rpisurv.service"
SYSTEMD_AUTO_ENABLE = "enable"


do_configure () {
	# Specify any needed configure commands here
	:
}

do_compile () {
	# Specify compilation commands here
	:
}

do_install () {

    install -d ${D}/usr/bin
    install -m 0744 ${S}/rpisurv ${D}/usr/bin/rpisurv


    install -d ${D}/usr/local/bin/rpisurv
    cp -R ${S}/surveillance/* ${D}/usr/local/bin/rpisurv



    if ${@bb.utils.contains('DISTRO_FEATURES','systemd','true','false',d)}; then
        install -d ${D}${systemd_unitdir}/system
        install -m 0644 ${S}/rpisurv.service ${D}${systemd_unitdir}/system/
        sed -i -e 's#@SBINDIR@#${sbindir}#g' ${D}${systemd_unitdir}/system/rpisurv.service
    fi

    install -m 0644 ${WORKDIR}/display1.yml ${D}/usr/local/bin/rpisurv/conf/
}
FILES:${PN} += "${libdir}"
FILES:${PN} += "/usr/local/bin/rpisurv/"

